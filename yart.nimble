# Package

version       = "0.1.0"
author        = "Justas Trimailovas"
description   = "yet another ray tracer"
license       = "MIT"
srcDir        = "src"
bin           = @["yart"]



# Dependencies

requires "nim >= 1.0.4"
