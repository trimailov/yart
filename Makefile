yart: src/yart.nim src/hitable.nim src/ray.nim src/vec3.nim
	nimble build yart
	chmod +x yart

image: yart
	./yart > image.ppm
	open image.ppm
