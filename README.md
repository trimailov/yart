# Yet Another Ray Tracer

Exercise with nim to implement "Ray Tracing in One Weekend"

## How to use

Makefile is provided, to run it:

```
make
```
