import math

type
  Vec3 * = tuple
    x, y, z: float

proc `+` *(v1: Vec3, v2: Vec3): Vec3 =
  return (x: v1.x + v2.x, y: v1.y + v2.y, z: v1.z + v2.z)

proc `-` *(v1: Vec3, v2: Vec3): Vec3 =
  return (x: v1.x - v2.x, y: v1.y - v2.y, z: v1.z - v2.z)

proc `*` *(v1: Vec3, v2: Vec3): Vec3 =
  return (x: v1.x * v2.x, y: v1.y * v2.y, z: v1.z * v2.z)

proc `*` *(v1: Vec3, f: float): Vec3 =
  return (x: v1.x * f, y: v1.y * f, z: v1.z * f)

proc `/` *(v1: Vec3, v2: Vec3): Vec3 =
  return (x: v1.x / v2.x, y: v1.y / v2.y, z: v1.z / v2.z)

proc `/` *(v1: Vec3, f: float): Vec3 =
  return (x: v1.x / f, y: v1.y / f, z: v1.z / f)

proc dot *(v1: Vec3, v2: Vec3): float =
  return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z

proc cross *(v1: Vec3, v2: Vec3): Vec3 =
  var x = v1.y * v2.z - v1.z * v2.y
  var y = -(v1.x * v2.z - v1.z * v2.x)
  var z = v1.x * v2.y - v1.y * v2.x
  return (x, y, z)

proc length *(v: Vec3): float =
  return sqrt(v.x * v.x + v.y * v.y + v.z * v.z)

proc unit_vector *(v: Vec3): Vec3 =
  return v / v.length();
