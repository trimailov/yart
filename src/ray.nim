import vec3

type
  Ray * = object
    a *, b *: Vec3

proc origin *(r: Ray): Vec3 =
  return r.a

proc direction *(r: Ray): Vec3 =
  return r.b

proc point_at_parameter *(r: Ray, t: float): Vec3 =
  return r.a + r.b * t
