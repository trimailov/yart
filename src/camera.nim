import ray, vec3

type
  Camera * = object
    origin *, lower_left_corner *, horizontal *, vertical *: Vec3


proc newCamera *(
    origin: Vec3 = (x: 0.0, y: 0.0, z: 0.0),
    lower_left_corner: Vec3 = (x: -2.0, y: -1.0, z: -1.0),
    horizontal: Vec3 = (x: 4.0, y: 0.0, z: 0.0),
    vertical: Vec3 = (x: 0.0, y: 2.0, z: 0.0),
  ): Camera =
  return Camera(origin: origin,
                lower_left_corner:
                lower_left_corner,
                horizontal: horizontal,
                vertical: vertical)


proc get_ray *(c: Camera, u: float, v: float): Ray {.inline.} =
  return Ray(a: c.origin, b: c.lower_left_corner + c.horizontal * u + c.vertical * v - c.origin)
