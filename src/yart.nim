import math
import random
import strformat

import camera, hitable, ray, vec3


proc color(r: Ray, world: Hitable): Vec3 =
  var rec = HitRecord()
  if world.hit(r, 0.0, 2.0^64, rec):
    return (x: rec.normal.x + 1, y: rec.normal.y + 1, z: rec.normal.z + 1) * 0.5
  else:
    let unit_direction: Vec3 = r.direction().unit_vector()
    let t: float = 0.5 * (unit_direction.y + 1.0)
    return (x: 1.0, y: 1.0, z: 1.0) * (1.0 - t) + (x: 0.5, y: 0.7, z: 1.0) * t


proc createImage() =
  let
    nx = 200
    ny = 100
    ns = 100

  echo("P3")
  echo(&"{nx} {ny}")
  echo("255")

  var list: seq[Hitable]
  list.add(Sphere(center: (x: 0.0, y: -100.5, z: -1.0), radius: 100.0))
  list.add(Sphere(center: (x: 0.0, y: 0.0, z: -1.0), radius: 0.5))
  let world = HitableList(list: list, list_size: list.len())

  let cam = newCamera()

  for j in countdown(ny, 0):
    for i in 0..<nx:
      var col: Vec3 = (x: 0.0, y: 0.0, z: 0.0)
      for s in 0..<ns:
        let u: float = (float(i) + rand(1.0)) / float(nx)
        let v: float = (float(j) + rand(1.0)) / float(ny)
        let r = cam.get_ray(u, v)

        col = col + color(r, world)

      col = col / float(ns)
      var ir = int(255.99 * col.x)
      var ig = int(255.99 * col.y)
      var ib = int(255.99 * col.z)
      echo(&"{ir} {ig} {ib}")


when isMainModule:
  randomize()
  createImage()
