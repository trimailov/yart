import math, system

import ray, vec3


type
  HitRecord * = ref object
    t *: float
    p *: Vec3
    normal *: Vec3

  Hitable * = ref object of RootObj

  Sphere * = ref object of Hitable
    center *: Vec3
    radius *: float

  HitableList * = ref object of Hitable
    list *: seq[Hitable]
    list_size *: int


method hit *(h: Hitable, r: Ray, t_min: float, t_max: float, hr: HitRecord): bool {.base.} =
  raise newException(Defect, "Not implemented")


method hit *(s: Sphere, r: Ray, t_min: float, t_max: float, hr: HitRecord): bool =
  # equation for spehere and ray intersection is this:
  # t*t*B.dot(B) + 2*t*(A-C).doct((A-C)) + C.dot(C) - R*R = 0
  #
  # t - is ray distance from the ray origin
  # A - ray origin vector
  # B - ray dihrtion vector
  # C - sphere center vector
  # R - sphere radius
  let oc: Vec3 = (r.origin() - s.center)
  let a: float = r.direction().dot(r.direction())
  let b: float = 2.0 * oc.dot(r. direction())
  let c: float = oc.dot(oc) - s.radius * s.radius
  let discriminant: float = b * b - 4 * a * c
  if discriminant > 0:
    var temp: float = (-b - sqrt(discriminant)) / (2 * a)
    if temp < t_max and temp > t_min:
      hr.t = temp
      hr.p = r.point_at_parameter(hr.t)
      hr.normal = (hr.p - s.center) / s.radius
      return true
    temp = (-b + sqrt(discriminant)) / (2 * a)
    if temp < t_max and temp > t_min:
      hr.t = temp
      hr.p = r.point_at_parameter(hr.t)
      hr.normal = (hr.p - s.center) / s.radius
      return true
  return false


method hit *(hl: HitableList, r: Ray, t_min: float, t_max: float, hr: HitRecord): bool =
  var hit_anything: bool = false
  for i in 0..<hl.list_size:
    if hl.list[i].hit(r, t_min, t_max, hr):
      hit_anything = true
  return hit_anything
